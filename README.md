## Read DS1307 real-time clock/calendar and display results

Using the dsPIC-EL-GM reads the clock cell from the Dallas DS1307
real-time clock/calendar on a shield and displays the result.

![display](images/tinyRTCdisplay.jpg)

**Display**

---

![schematic](images/tinyRTC8.png)

**Schematic**

---

