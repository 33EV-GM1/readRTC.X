/*! \file  i2c.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date September 19, 2015, 8:35 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include "i2c.h"


/*! Initializes the I2C peripheral.   */
/*! This function sets the I2C baud rate, sets up master mode with
 *  no slew rate control, clears the transmit and receive
 *  registers and then enables the I2C peripheral.  The baud rate
 *  is calculated in i2c.h.
 *
 *  \param none
 *  \return none
 */
void InitI2C(void)
{
  /* Neither RB5 nor RB6 have analog capability  */
  _TRISB5 = 0;
  _TRISB6 = 0;
  
    /* Now we will initialize the I2C peripheral for Master Mode,
     *  No Slew Rate Control, and leave the peripheral switched off.
     *
     * bit name   val meaning
     *  15 I2CEN   0  Enable (disable for now)
     *  14 unused  0
     *  13 I2CSIDL 0  Stop in idle mode (no)
     *  12 SCLREL  0  Release control bit (no - slave only)
     *  11 IPMIEN  0  IPMI enable (no)
     *  10 A10M    0  10-bit slave address (no - slave only)
     *   9 DISSLW  1  Disable slew rate control (yes)
     *   8 SMEN    0  SMBbus input levels (no)
     *   7 GCEN    0  General call enable (no - slave only)
     *   6 STREN   0  Clock stretch bit (no - slave only)
     *   5 ACKDT   0  Acknowledge data bit (no)
     *   4 ACKEN   0  Acknowledge sequence enable (no)
     *   3 RCEN    0  Receive enable (no)
     *   2 PEN     0  Stop condition enable (no)
     *   1 RSEN    0  Repeated start condition enable (no)
     *   0 SEN     0  Start condition enable (no)
     */
    I2C1CON1 = 0x0200;

    /* Set the I2C BRG Baud Rate.  The value for BRGVAL is
     * calculated in i2c.h */
    I2C1BRG = BRGVAL;

    /* Clear the receive and transmit buffers */
    I2C1RCV = 0x0000;
    I2C1TRN = 0x0000;

    /* Now we can enable the peripheral */
    I2C1CON1bits.I2CEN = 1;
}


/*! Generates an I2C Start Condition.   */
/*! This function generates an I2C start condition and returns status
 *  of the Start.
 *
 *  \param none
 *  \return I2C1STATbits.S - Start condition detected
 */
unsigned int StartI2C(void)
{
    IdleI2C();                      /* Wait for bus idle            */
    I2C1CON1bits.SEN = 1;             /* Generate Start Condition     */
    while (I2C1CON1bits.SEN);         /* Wait for Start Condition     */
    return(I2C1STATbits.S);          /* Optionally return status     */
}


/*! Writes a byte out to the bus   */
/*! This function transmits the byte passed to the function
 *
 *  \param byte (unsigned char) byte to be written
 *  \return none
 *
 */
void WriteI2C(unsigned char byte)
{
    IdleI2C();                      /* Wait for bus to be idle          */
    I2C1TRN = byte;                  /* Load byte to I2C Transmit buffer */
    while (I2C1STATbits.TBF);        /* wait for data transmission       */
}


/*! Generates a bus stop condition.  */
/*! This function generates an I2C stop condition and returns status
 *  of the Stop.
 *
 *  \param none
 *  \return I2C1STATbits.P - stop condition detected bit
 */
unsigned int StopI2C(void)
{
    IdleI2C();
    I2C1CON1bits.PEN = 1;             /* Generate stop condition      */
    while (I2C1CON1bits.PEN);         /* Wait for Stop                */
    return(I2C1STATbits.P);          /* return status                */
}


/*! Waits for bus to become Idle.   */
/*! Watches the transmit status bit and exits when the
 *  bit is cleared.
 *
 *  Technically this doesn't mean the bus is clear, but the part cannot
 *  transmit while a receive is in progress, so essentially, ensuring the
 *  transmit buffer is full is adequate.
 *
 *  \param none
 *  \return none
 */
void IdleI2C(void)
{
    while (I2C1STATbits.TRSTAT);     /* Wait for bus Idle        */
}


/*! Return the Acknowledge status on the bus   */
/*! This function checks the state of the ACKSTAT bit in the I2C
 *  status register and returns TRUE if clear.
 *
 *  \param none
 *  \return Inverse of state of I2C1STATbits.ACKSTAT
 *
 */
unsigned int ACKstatusI2C( void )
{
    return (!I2C1STATbits.ACKSTAT);
}

/*! Read a single byte from Bus.  */
/*! Enables master receive then waits for the receive buffer to be full.
 *
 *  \param none
 *  \return Contents of I2C receive buffer
 *
 */
unsigned int getI2C(void)
{
    IdleI2C();
    I2C1CON1bits.RCEN = 1;            /* Enable Master receive            */
    Nop();
    while(!I2C1STATbits.RBF);        /* Wait for receive bufer to be full*/
    return(I2C1RCV);                 /* Return data in buffer            */
}


/*! Generates a restart condition and returns the "Start heard last" bit.   */
/*! Function sets the restart bit and waits for the restart to happen. It
 *  then returns I2C1STATbits.S, the "Start heard last" bit.
 *
 *  \param none
 *  \return I2C1STATbits.S - start condition detected
 */
unsigned int RestartI2C(void)
{
    IdleI2C();
    //1 IFS0bits.MI2CIF = 0;
    I2C1CON1bits.RSEN = 1;            /* Generate restart             */
    while (I2C1CON1bits.RSEN);        /* Wait for restart             */
    //while ( !IFS0bits.MI2CIF);
    return(I2C1STATbits.S);          /* return status                */
}

/*! Generates a NO Acknowledge on the Bus   */
/*! Sets the NO-ACK bit, then ACK enable and then waits for
 *  the NAK to complete.
 *
 *  \param none
 *  \return none
 */
void NotAckI2C(void)
{
    IdleI2C();
    I2C1CON1bits.ACKDT = 1;                   //Set for NotACk
    I2C1CON1bits.ACKEN = 1;
    while(I2C1CON1bits.ACKEN);                //wait for ACK to complete
    I2C1CON1bits.ACKDT = 0;                   //Set for NotACk
}


/*! Read a byte from the DS1307. */
/*! This function reads a byte from the DS1307 FRAM.
 *
 *  \param ucDevice   unsigned char - DS1307 device address
 *  \param uAddress   unsigned int  - Address of location to read
 *  \return unsigned char contents of memory cell
 *
 */
unsigned char DS1307readByte( unsigned char ucDevice,
                              unsigned int  uAddress )
{
    unsigned char ucControlByte;
    unsigned char ucLowAddr;
    unsigned char ucResult;

    /* Write condition is a zero bit so the control byte is formed
     * merely by shifting the address left one bit.  However, on the
     * DS1307 the control byte includes the high three bits of the
     * memory address as bits 1, 2, and 3, with bit 0 still 0 for write */
    ucControlByte = ( ucDevice<<1 /* & 0xf0 */ ) /*| ( (uAddress & 0x700)>>7 )*/;
    ucLowAddr = uAddress & 0xff;

    StartI2C();                 /* Start I2C transaction            */
    WriteI2C( ucControlByte );  /* Send bus Address                 */
    WriteI2C(ucLowAddr);        /* Address of desired register      */
    RestartI2C();               /* Restart so can send read         */
    WriteI2C( ucControlByte+1 );/* Send bus address with write      */
    ucResult = getI2C();        /* Get answer from DS1307        */
    NotAckI2C();                /* NAK result to stop answers       */
    StopI2C();                  /* Send stop on bus                 */

    return ( ucResult );
}



/*! Write a value to the DS1307 */
/*! Writes a byte to the DS1307.
 *
 *  \param  ucDevice unsigned char Device I2C address
 *  \param  uAddress unsigned int  Address of memory cell to write
 *  \param  uValue   unsigned char Value to write to the DS1307
 *  \return none
 */
void DS1307writeByte( unsigned char ucDevice,
                         unsigned int  uAddress,
                         unsigned char ucValue )
{
    unsigned char ucControlByte;
    unsigned char ucLowAddr;

    /* Write condition is a zero bit so the control byte is formed
     * merely by shifting the address left one bit.  However, on the
     * DS1307 the control byte includes the high three bits of the
     * memory address as bits 1, 2, and 3, with bit 0 still 0 for write */
    ucControlByte = ( ucDevice<<1 /*& 0xf0*/ ) /*| ( (uAddress & 0x700)>>7 )*/;
    ucLowAddr = uAddress & 0xff;
    
    StartI2C();         /* Start I2C transaction            */
    WriteI2C(ucControlByte); /* Address of DS1307 | write  */
    WriteI2C(ucLowAddr);/* high 4 bits of value             */
    WriteI2C(ucValue);  /* Low 8 bits of value              */    
    StopI2C();          /* Stop the transaction             */
}
