/*! \file  readRTC.c
 *
 *  \brief Display the time and date from the RTC
 *
 *
 *  \author jjmcd
 *  \date September 20, 2015, 3:10 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include <stdio.h>
#include "../include/LCD.h"
#include "../include/DS1307.h"

// Fast RC oscillator with PLL
#pragma config FNOSC = FRCPLL
// Watchdog timer off
#pragma config FWDTEN = OFF
// Deadman timer off
#pragma config DMTEN = DISABLE

/*! Storage for RTC time contents */
unsigned char ucResults[8];
/*! Days of the week for display */
char szWeekdays[8][4] = {"Sun", "Mon", "Tue", "Wed", "Thu",
  "Fri", "Sat","Sun"};

/*! main - Display the date and time */
/*! Reads the DS1307 real-time clock/calendar and displays
 *  the result in human-readable format
 */
int main(void)
{
  unsigned char ucRTCcell;  /*!< Address within DS1307              */
  int nFlashCycle;          /*!< Counter to make flashing square    */
  char szWork[32];          /*!< String for display                 */

  /* Set up clock for 70 MIPS (140 MHz) */
  CLKDIVbits.FRCDIV = 0; // Divide by 1 = 8MHz
  CLKDIVbits.PLLPRE = 0; // Divide by 2 = 4 MHz
  PLLFBD = 74; // Multiply by 70 = 140
  CLKDIVbits.PLLPOST = 0; // Divide by 2 = 70

  /* Show some start up stuff on the LCD just because */
  LCDinit();
  LCDclear();
  LCDputs("    Read RTC    ");
  Delay_ms(1000);
  
  /* Initialize the I2C peripheral */
  I2Cinit();

  /* And the counter for flashing */
  nFlashCycle = 0;
  _TRISB4 = 0;
  LCDclear();
  
  while (1)
    {
      /* Read the clock registers */
      for (ucRTCcell = 0; ucRTCcell < 8; ucRTCcell++)
        ucResults[ucRTCcell] = DS1307readByte(DS1307_ADDRESS, 
                (unsigned int) ucRTCcell);

      /* Display the date */
      sprintf(szWork, "20%02x-%02x-%02x", ucResults[6],
              ucResults[5], ucResults[4]);
      LCDhome();
      LCDputs(szWork);

      /* Display the time */
      sprintf(szWork, "%02x:%02x:%02x", ucResults[2]&0x3f,
              ucResults[1], ucResults[0]);
      LCDline2();
      LCDputs(szWork);

      /* Display the day of the week */
      LCDposition(0x4b);
      LCDletter(ucResults[3]+0x30);
      LCDposition(0x4d);
      LCDputs(szWeekdays[ucResults[3]]);

      /* Display a flashing square so we know it's working */
      nFlashCycle++;
      if (nFlashCycle > 9999)
        nFlashCycle = 1;
      LCDposition(15);
      if (nFlashCycle & 1)
        {
          _LATB4 = 1;
          LCDletter(0xdb);
        }
      else
        {
          LCDletter(' ');
        }
      _LATB4 = 0;
      Delay_ms(100);
    }
  return 0;
}
